﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_6 : Location
    {
        bool JohnForgotToLockTheGate = false;
        bool dogWantsToRunAway = false;

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Внушить Джону забывчивость", Forget, true, true));
            actions.Add(new GameAction("Внушить собаке Дона желание убежать", DogRunAway, true, true));
        }

        public Location1_6()
        {
            ShowMessage("Тобиас и Коди проходят мимо дома Джона Блэка. Он возвращается домой после прогулки со своей собакой.");
            AskForActions();
            if(dogWantsToRunAway)
            {
                ShowMessage("Собака Джона пытается убежать, но Джон крепко держит поводок и не даёт её этого сделать. Собака больше не хочет убегать.");
                dogWantsToRunAway = false;
            }
            if(JohnForgotToLockTheGate)
            {
                ShowMessage("Джон заходит во двор, забыв закрыть калитку, снимает собаку с поводка и идёт домой");
            }
            else
            {
                ShowMessage("Джон заходит во двор, закрывает калитку, снимает собаку с поводка и идёт домой");
            }
            AskForActions();
            if (dogWantsToRunAway)
            {
                if (JohnForgotToLockTheGate)
                {
                    ShowMessage("Собака Джона убегает через открытую калитку, Джон бежит ловить её. Собака бежит от него.");
                    Level1.Instance.JohnSaved = true;
                }
                else
                {
                    ShowMessage("Собака пытается убежать, но не может из-за закрытой калитки");
                }
            }
            else
                ShowMessage("Джон идёт домой, собака остаётся сидеть во дворе");
            Level1.Instance.AddMinutes(5);
            new Location1_7();
        }

        private void Forget()
        {
            JohnForgotToLockTheGate = true;
        }

        private void DogRunAway()
        {
            dogWantsToRunAway = true;
        }
    }
}
