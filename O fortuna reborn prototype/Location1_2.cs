﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_2 : Location
    {
        bool treeDroped = false;

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Повалить дерево", delegate { ShowMessage("Дерево упало"); treeDroped = true; }, true, true));
        }

        public Location1_2()
        {
            ShowMessage("Тобиас и коди проходят развилку");
            AskForActions();
            if (treeDroped)
            {
                ShowMessage("Упавшее дерево преградило путь, Тобиас решил пойти другой дорогой.");
                new Location1_4();
            }
            else
                new Location1_3();
        }

    }
}
