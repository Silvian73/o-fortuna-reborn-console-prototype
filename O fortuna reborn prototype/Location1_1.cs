﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_1 : Location
    {
        private bool plywoodDropped = false;
        private bool lanternDropped = false;

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Положить фанеру на яму", DropPlywood, true, true));
            actions.Add(new GameAction("Уронить фонарь в яму", DropLantern, true, true));
        }

        public Location1_1() : base()
        {
            Level1.Instance.ShowTime();
            ShowMessage("Тобиас и Коди выходят из дома. Впереди них яма.");
            AskForActions();
            if(lanternDropped)
            {
                ShowMessage("Тобиас заметил яму и обошёл её");
                Level1.Instance.AddMinutes(5);
                new Location1_5();
            }
            else
            {
                if (plywoodDropped)
                    ShowMessage("Тобиас пошёл по фанере, но под его весом она сломалась, и Тобиас упал в яму");
                else
                    ShowMessage("Тобиас не заметил яму и упал в неё");
                ShowMessage("Тобиас получил серьёзную травму, прохожие вызвали скорую, и его отвезли в больницу, которая к несчастью была около железной дороги");
                Level1.Instance.Ending();
            }
        }

        private void DropPlywood()
        {
            plywoodDropped = true;
            ShowMessage("Подул ветер, и кусок фанеры упал, закрывая собой яму.");
        }

        private void DropLantern()
        {
            lanternDropped = true;
            ShowMessage("Подул ветер, и еле-еле стоящий фонарь c грохотом упал в яму, напугав Тобиаса и Коди.");
            if (plywoodDropped)
                ShowMessage("Фанера сломалась под падающим фонарём.");
        }
    }
}
