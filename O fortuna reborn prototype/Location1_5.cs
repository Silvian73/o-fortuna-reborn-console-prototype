﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_5 : Location
    {
        bool courage = false;

        public Location1_5() : base()
        {
            ShowMessage("Путь Тобиасу и Коди преграждает свора бродячих собак. Они выглядят угрожающе");
            AskForActions();
            if (courage)
            {
                ShowMessage("Коди смело залаял на собак, и они ушли. Тобиас и Коди пошли дальше.");
                Level1.Instance.AddMinutes(5);
                new Location1_2();
            }
            else
            {
                ShowMessage("Тобиас и Коди убежали");
                Level1.Instance.AddMinutes(10);
                new Location1_6();
            }
        }

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Внушить Коди храбрость", Courage, true, true));
        }

        private void Courage()
        {
            courage = true;
        }
    }
}
