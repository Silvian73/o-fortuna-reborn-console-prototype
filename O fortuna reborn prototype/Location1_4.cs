﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Location1_4 : Location
    {
        bool tobiasWantsTohelp = false;

        protected override void InitializeActions()
        {
            actions.Add(new GameAction("Внушить Тобиасу желание помочь", delegate { tobiasWantsTohelp = true; }, true, true));
        }

        public Location1_4()
        {
            ShowMessage("Тобиас и коди проходят мимо дома семьи Стивенсонов. Они собрались уезжать, но не могут найти кошелёк.");
            AskForActions();
            if(tobiasWantsTohelp)
            {
                ShowMessage("Тобиас поинтересовался, что не так и предложил свою помощь. С его помощью Стивенсоны нашли кошелёк и уехали");
                Level1.Instance.AddMinutes(5);
                Level1.Instance.StevensonsSaved = true;
            }
            ShowMessage("Тобиас и Коди идут дальше");
            Level1.Instance.AddMinutes(5);
            new Location1_6();
        }
    }
}
