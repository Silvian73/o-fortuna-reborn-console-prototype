﻿using System;
using System.Collections.Generic;
using System.Text;

namespace O_fortuna_reborn_prototype
{
    class Level1
    {
        public static Level1 Instance;

        private int hours = 9;
        private int minutes = 25;

        public void AddMinutes(int value, bool show = true)
        {
            minutes += value;
            while(minutes >= 60)
            {
                minutes -= 60;
                hours++;
            }
            if(show)
                ShowTime();
        }

        public bool TrainGone => !(hours == 9 && minutes <= 56);

        public void ShowTime() => ShowMessage($"Текущее время {hours}:{minutes}");

        private IPresenter Presenter => Game.Presenter;

        private void ShowMessage(string message) => Presenter.ShowMessage(message);

        public bool TobiasSaved = false;
        public bool StevensonsSaved = false;
        public bool JohnSaved = false;

        public Level1()
        {
            Instance = this;
            Intro();
            new Location1_1();
        }

        private void Intro()
        {
            ShowMessage("Уровень 1. На город надвигается опасность. В 10:10 поезд сойдёт с рель и рухнет на город. У двенадцатилетнего мальчика Тобиаса " +
                "сегодня выходной, и он собирается на поезде выехать на природу и погулять с друзьямии и своим псом Коди. Его поезд отправляется в 9:58, чтобы успеть на поезд," +
                "он должен прийти на станцию хотя бы в 9:56");
        }

        public void Ending()
        {
            ShowMessage($"На огромной скорости поезд сходит с рельс и падает на город, уничтожая всё на своём пути. Тобиас {(TobiasSaved ? "выжил" : "погиб")}, " +
                $"Стивенсоны {(StevensonsSaved ? "выжили" : "погибли")}, Джон {(JohnSaved ? "выжил" : "погиб")}.");
            if (TobiasSaved && StevensonsSaved && JohnSaved)
                ShowMessage("Поздравляем, вы открыли истинную концовку");
        }
    }
}
